using UnityEngine;
using System.Collections;
using System;

namespace TankU
{
	[RequireComponent(typeof(CharacterController))]
	[RequireComponent(typeof(IVehiclePartProvider))]
	public class TankMovement : Photon.MonoBehaviour, ICameraLimitsProvider
	{
		public EngineDataClass engineData;
		public TurretDataClass turretData;

		private CharacterController characterController;
		private HealthComponent healthComponent;

		private Transform tankChassis;
		private Vector3 currentMovement;
		private float currentTurnSpeed;
		private bool isMoving = false;

		void Start()
		{
			characterController = GetComponent<CharacterController>();
			IVehiclePartProvider partProvider = (IVehiclePartProvider)GetComponent(typeof(IVehiclePartProvider));
			tankChassis = partProvider.getChassisTransform();

			healthComponent = GetComponent<HealthComponent>();

			PlayerCamera playerCamera = GetComponent<PlayerCamera>();

			if (playerCamera != null)
			{
				if (photonView.isMine || PhotonNetwork.offlineMode)
				{
					playerCamera.OnStartFollowing();
				}
			}
			else
			{
				Debug.Log("Error no Player Camera attached");
			}
		}

		void Update()
		{
			if (photonView.isMine == false && PhotonNetwork.connected == true)
			{
				return;
			}

			ResetSpeedValues();
			UpdateForwardMovement();
			UpdateBackwardMovement();
			UpdateRotateMovement();
			MoveCharacterController();
			ApplyGravityToCharacterController();
			UpdateGroundNormal();
		}

		void ResetSpeedValues()
		{
			currentMovement = Vector3.zero;
			currentTurnSpeed = 0;
			isMoving = false;
		}

		void UpdateForwardMovement()
		{
			if (healthComponent.isDead)
			{
				return;
			}

			if (Input.GetKey(KeyCode.W) || Input.GetAxisRaw("Vertical") > 0.1f)
			{
				currentMovement = transform.forward * engineData.speed;
				isMoving = true;
			}
		}

		void UpdateBackwardMovement()
		{
			if (healthComponent.isDead)
			{
				return;
			}

			if (Input.GetKey(KeyCode.S) || Input.GetAxisRaw("Vertical") < -0.1f)
			{
				currentMovement = -transform.forward * engineData.reversingSpeed;
				isMoving = true;
			}
		}

		void UpdateRotateMovement()
		{
			if (healthComponent.isDead)
			{
				return;
			}

			if (Input.GetKey(KeyCode.A) || Input.GetAxisRaw("Horizontal") < -0.1f)
			{
				currentTurnSpeed = isMoving ? engineData.turningSpeed : engineData.rotatingSpeed;
				transform.Rotate(0.0f, -currentTurnSpeed * Time.deltaTime, 0.0f);
			}

			if (Input.GetKey(KeyCode.D) || Input.GetAxisRaw("Horizontal") > 0.1f)
			{
				currentTurnSpeed = isMoving ? engineData.turningSpeed : engineData.rotatingSpeed;
				transform.Rotate(0.0f, currentTurnSpeed * Time.deltaTime, 0.0f);
			}
		}

		void ApplyGravityToCharacterController()
		{
			characterController.Move(transform.up * Time.deltaTime * -9.81f);
		}

		void MoveCharacterController()
		{
			characterController.Move(currentMovement * Time.deltaTime);
		}

		void UpdateGroundNormal()
		{
			RaycastHit hitInfo;
			if (Physics.Raycast(transform.position, -transform.up, out hitInfo))
			{
				var forward = Vector3.Cross(-hitInfo.normal, transform.right);
				var oldRotation = tankChassis.rotation;
				var newRotation = Quaternion.LookRotation(forward, hitInfo.normal);
				tankChassis.rotation = Quaternion.Lerp(oldRotation, newRotation, Time.deltaTime * 10.0f);
			}
		}

		float ICameraLimitsProvider.GetMaxElevation()
		{
			return turretData.maxElevation;
		}

		float ICameraLimitsProvider.GetMinElevation()
		{
			return turretData.minElevation;
		}

		float ICameraLimitsProvider.GetMaxRotation()
		{
			return turretData.maxRotation;
		}
	}
}