using UnityEngine;
using System.Collections;

namespace TankU
{
	public class PlayerCamera : Photon.MonoBehaviour
	{
		public GameObject player;

		private Transform root;
		private Transform turret;
		private Transform barrel;

		public CameraOrbitDataClass cameraOrbitData;

		public LayerMask rayCastFilter;

		public Transform cameraTransform;
		internal Camera cameraObject;
		public float cameraFOV = 50.0f;

		// Camera movement specs
		private float horizontalSpeed;
		private float verticalSpeed;
		private float orbitHeight;
		private float orbitDistance;

		// Camera limits that depends on vehicle specs
		private float maxOrbitLeft;
		private float maxOrbitRight;
		private float minElevation;
		private float maxElevation;

		private float inputOrbitY = 0.0f;
		private float deltaInputOrbitX = 0.0f;

		private Vector3 centerOfTheScreen;
		private Vector3 orbitPosition;
		private Quaternion orbitRotation;
		public Vector3 aimPoint;
		private bool isFollowing = false;

		private void InitCamera()
		{
			cameraTransform = GameObject.FindGameObjectWithTag("PlayerCamera").transform;
			cameraObject = Camera.main.GetComponentInChildren<Camera>();
			cameraObject.fieldOfView = cameraFOV;
			horizontalSpeed = cameraOrbitData.horizontalSpeed * PlayerPrefs.GetFloat("HorizontalSensitivity", 1.0f);
			verticalSpeed = cameraOrbitData.verticalSpeed * PlayerPrefs.GetFloat("VerticalSensitivity", 1.0f);
			orbitHeight = cameraOrbitData.height;
			orbitDistance = cameraOrbitData.distance;
		}

		private void RefreshPlayer()
		{
			player = this.gameObject;
		}

		private void RefreshVehicleSpecs()
		{
			IVehiclePartProvider partProvider = (IVehiclePartProvider)player.GetComponent(typeof(IVehiclePartProvider));
			if (partProvider != null)
			{
				root = partProvider.getRootTransform();
				turret = partProvider.getTurretTransform();
				barrel = partProvider.getBarrelTransform();
			}

			ICameraLimitsProvider limitsProvider = (ICameraLimitsProvider)player.GetComponent(typeof(ICameraLimitsProvider));
			if (limitsProvider != null)
			{
				maxOrbitRight = limitsProvider.GetMaxRotation();
				maxOrbitLeft = -maxOrbitRight;
				maxElevation = limitsProvider.GetMaxElevation();
				minElevation = limitsProvider.GetMinElevation();
			}
		}

		public void OnStartFollowing()
		{
			InitCamera();
			UpdateScreenResolution();

			RefreshPlayer();

			if (!player)
			{
				return;
			}

			RefreshVehicleSpecs();

			isFollowing = true;
		}

		void Update()
		{
			if (!isFollowing)
			{
				return;
			}

			if (!player)
			{
				return;
			}

			UpdateScreenResolution();
			GetInputs();
		}

		void LateUpdate()
		{
			if (!isFollowing)
			{
				return;
			}

			if (!player)
			{
				return;
			}

			GetCameraTarget();
			ApplyOrbit();
			AdjustBarrelAim();
		}

		void UpdateScreenResolution()
		{
			centerOfTheScreen = new Vector3(cameraObject.pixelWidth / 2, cameraObject.pixelHeight / 2, 1);
		}

		private void GetInputs()
		{
			deltaInputOrbitX = Input.GetAxis("Mouse X") * horizontalSpeed * Time.deltaTime;
			inputOrbitY -= Input.GetAxis("Mouse Y") * verticalSpeed * Time.deltaTime;
			inputOrbitY = Mathf.Clamp(inputOrbitY, minElevation, maxElevation);
		}

		private void GetCameraTarget()
		{
			RaycastHit hit;
			Ray ray = cameraObject.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

			float cameraToPlayerDistance = Vector3.Distance(cameraObject.transform.position, player.transform.position);

			if (Physics.Raycast(ray, out hit, 100, rayCastFilter) && hit.distance > cameraToPlayerDistance)
			{
				aimPoint = hit.point;
			}
			else
			{
				aimPoint = ray.origin + (ray.direction * 100);
			}
		}

		private void ApplyOrbit()
		{
			// Horizontal component of movement
			float turretAngle = turret.localEulerAngles.y + deltaInputOrbitX;
			// Clamp turret horizontal rotation
			turretAngle = turretAngle > maxOrbitRight && turretAngle < 180 ? maxOrbitRight : turretAngle;
			turretAngle = turretAngle < 360 + maxOrbitLeft && turretAngle > 180 ? 360 + maxOrbitLeft : turretAngle;
			turret.localRotation = Quaternion.Euler(0, turretAngle, 0);
			// Sum body and turret rotation components
			float horizontalOrbit = root.localRotation.eulerAngles.y + turret.localRotation.eulerAngles.y;
			// Calculate final rotation and position
			orbitRotation = Quaternion.Euler(inputOrbitY, horizontalOrbit, 0.0f);
			orbitPosition = orbitRotation * new Vector3(0.0f, orbitHeight, -orbitDistance) + root.position;
			// Apply to the transform
			cameraTransform.rotation = orbitRotation;
			cameraTransform.position = orbitPosition;
		}

		private void AdjustBarrelAim()
		{
			barrel.LookAt(aimPoint);
		}
	}
}
