﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class TankShooting : Photon.MonoBehaviour, IWeaponSpecsProvider//, IPunObservable
	{
		public WeaponDataClass weaponData;
		public BulletDataClass bulletData;

		private Transform barrelOut;
		private float fireTime = 0.0f;
		private AudioSource shootingAudioSource;
		private HealthComponent healthComponent;

		private void Awake()
		{
			if (!shootingAudioSource)
			{
				shootingAudioSource = gameObject.AddComponent<AudioSource>();
			}


		}

		void Start()
		{
			healthComponent = GetComponent<HealthComponent>();
			RefreshPlayer();
		}

		private void RefreshPlayer()
		{
			IVehiclePartProvider partProvider = (IVehiclePartProvider)GetComponent(typeof(IVehiclePartProvider));
			if (partProvider != null)
			{
				barrelOut = partProvider.getBarrelOutTransform();
			}
		}

		IEnumerator Fire()
		{
			fireTime = Time.time + weaponData.coolDownTime;
			//shootingAudioSource.clip = weaponData.fireClips[Random.Range(0, weaponData.fireClips.Length)];
			shootingAudioSource.PlayOneShot(weaponData.fireClips[Random.Range(0, weaponData.fireClips.Length)]);
			GameObject bullet = Instantiate(bulletData.bullet, barrelOut.position, barrelOut.rotation) as GameObject;
			BulletBehaviour bulletBehaviour = bullet.GetComponent<BulletBehaviour>();
			bulletBehaviour.bulletOwner = photonView.owner.ID;
			GameObject effect = Instantiate(weaponData.fireEffect, barrelOut.position, barrelOut.rotation) as GameObject;
			Destroy(effect, weaponData.fireEffectLifeTime);
			yield return null;
		}

		// Update is called once per frame
		void Update()
		{
			if (photonView.isMine == false && PhotonNetwork.connected == true)
			{
				return;
			}

			if (healthComponent.isDead)
			{
				return;
			}

			if (Input.GetButton("Fire1") && Time.time >= fireTime)
			{
				StartCoroutine("Fire");
				photonView.RPC("FireRPC", PhotonTargets.Others);
			}
		}

		float IWeaponSpecsProvider.GetRange()
		{
			return weaponData.range;
		}

		//void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		//{
		//	if (stream.isWriting)
		//	{


		//	}
		//	else
		//	{

		//	}
		//}

		[PunRPC]
		void FireRPC(PhotonMessageInfo info)
		{
			if (photonView.viewID == info.photonView.viewID)
			{
				StartCoroutine("Fire");
			}
			Debug.Log(string.Format("Info: {0} {1} {2}", info.sender, info.photonView, info.timestamp));
		}
	}
}
