﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class MachineGunShooting : MonoBehaviour
	{
		public AudioSource m_ShootingAudio;
		public AudioClip m_FireEndClip;
		public AudioClip m_FireClip;
		public GameObject fireEffect;
		public Transform barrelOut;

		public float cooldownTime = 1.0f;
		private float fireTime = 0.0f;

		public float audioLoop;
		public float effectLoop;


		void Start()
		{
			//IVehiclePartProvider partProvider = (IVehiclePartProvider)GetComponent(typeof(IVehiclePartProvider));
			//if (partProvider != null)
			//{
			//	root = partProvider.getRootTransform();
			//	turret = partProvider.getTurretTransform();
			//	barrel = partProvider.getBarrelTransform();
			//}
		}

		IEnumerator FireAudio()
		{
			m_ShootingAudio.clip = m_FireClip;
			m_ShootingAudio.loop = true;
			m_ShootingAudio.Play();

			yield return null;
		}

		IEnumerator FireFX()
		{
			GameObject effect = Instantiate(fireEffect, barrelOut.position, barrelOut.rotation) as GameObject;
			effect.transform.SetParent(barrelOut);

			while (Input.GetButton("Fire1"))
			{
				yield return new WaitForSeconds(effectLoop);

				effect.SetActive(false);
				effect.SetActive(true);
				yield return null;
			}

			Destroy(effect, 1.0f);
		}

		// Update is called once per frame
		void Update()
		{
			if (Input.GetButtonDown("Fire1"))
			{
				// check ammo
				StartCoroutine("FireAudio");
				StartCoroutine("FireFX");
			}

			if (Input.GetButtonUp("Fire1")) // or out of ammo
			{
				m_ShootingAudio.Stop();
				m_ShootingAudio.loop = false;
				m_ShootingAudio.clip = m_FireEndClip;
				m_ShootingAudio.Play();

			}
		}
	}
}
