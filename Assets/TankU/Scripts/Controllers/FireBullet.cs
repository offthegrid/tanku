﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet: MonoBehaviour {

	public Rigidbody bulletPrefab;
	public Transform barrelOut;
	public float shootForce;
	public float coolDownTime = 3.0f;
	private float coolDown = 0.0f;
	bool isCoolingDown = false;


	// Use this for initialization
	void Start () {
		
	}

	private void Fire()
	{
		isCoolingDown = true;
		Rigidbody shellInstance = Instantiate(bulletPrefab, barrelOut.position, barrelOut.rotation) as Rigidbody;

		// Set the shell's velocity to the launch force in the fire position's forward direction.
		shellInstance.velocity = shootForce * barrelOut.forward;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(isCoolingDown)
		{
			coolDown += Time.deltaTime;
		}

		else if (Input.GetButtonDown("Fire1"))
		{
			Fire();
		}

		if(coolDown >= coolDownTime)
		{
			isCoolingDown = false;
			coolDown = 0.0f;
		}
	}
}
