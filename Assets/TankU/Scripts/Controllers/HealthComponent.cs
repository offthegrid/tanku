﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace TankU
{
	public class HealthComponent : Photon.PunBehaviour, IPunObservable
	{
		public int health = 100;
		public bool isDead = false;
		public bool inDeadSequence = false;
		public float respawnWaitTime = 5.0f;
		public GameObject deadSequence;
		public Material burningMaterial;

		public PlayerHUDController hudController;

		void Awake()
		{
			hudController = null;
		}

		void Start()
		{
			hudController = GameObject.FindGameObjectWithTag("HudController").GetComponent<PlayerHUDController>();
		}

		void Update()
		{
			if (isDead && !inDeadSequence)
			{
				StartCoroutine("DeadSequence");
				inDeadSequence = true;
			}
		}

		IEnumerator DeadSequence()
		{
			// show dead tank
			// block input
			// show UI on local player
			deadSequence.SetActive(true);
			SetBurningMaterial();
			yield return new WaitForSeconds(respawnWaitTime);

			if (photonView.isMine)
			{
				GameManager.Instance.UnspawnThenRespawn();
			}
		}

		IEnumerator ApplyDamage(object[] parms) // 0 = int damage, 1 = int bulletOwner
		{
			int damage = (int)parms[0];
			int bulletOwner = (int)parms[1];

			health -= damage;
			if (health < 0)
			{
				health = 0;
				isDead = true;

				// if we are the owners of the killing bullet...increase score
				if(isDead && PhotonNetwork.player.ID == bulletOwner)
				{
					// if we didn't kill a friend
					if(PhotonNetwork.player.GetTeam() != photonView.owner.GetTeam())
					{
						GameManager.Instance.IncreaseTeamScore(PhotonNetwork.player.GetTeam());
						PhotonNetwork.player.SetScore(PhotonNetwork.player.GetScore() + 1);
					}

					if(hudController)
					{
						hudController.PublishYouKilledMessage(photonView.owner.NickName);
						Debug.Log(PhotonNetwork.player.NickName + " killed " + photonView.owner.NickName);
					}
					
				}
			}
			yield return null;
		}

		private void SetBurningMaterial()
		{
			IVehiclePartProvider partProvider = (IVehiclePartProvider)GetComponent(typeof(IVehiclePartProvider));
			var mats = partProvider.getChassisTransform().gameObject.GetComponent<Renderer>().materials;
			mats[0] = burningMaterial;
			partProvider.getChassisTransform().gameObject.GetComponent<Renderer>().materials = mats;
		}

		void OnGUI()
		{
			if (photonView.isMine && hudController)
			{
				hudController.OnUpdateHealth(health);
			}
		}

		// Replicating the life
		void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		{
			if (stream.isWriting)
			{
				//// We own this player: send the others our data
				//stream.SendNext(health);
				//Debug.Log("Sending out health " + health);
			}
			else
			{
				//// Network player, receive data
				//this.health = (int)stream.ReceiveNext();

				//Debug.Log(string.Format("Info: {0} {1} {2}", info.sender, info.photonView, info.timestamp));
			}
		}

		[PunRPC]
		void ReceiveDamage(int damage, int bulletOwner, PhotonMessageInfo info)
		{
			// it's the same health component we hit on Master Client
			if (photonView.viewID == info.photonView.viewID)
			{
				object[] parms = new object[] { damage, bulletOwner };
				StartCoroutine("ApplyDamage", parms);
			}
			//Debug.Log(string.Format("Info: {0} {1} {2}", info.sender, info.photonView, info.timestamp));
		}
		//public void ReceiveDamageNow(int damage)
		//{
		//	StartCoroutine("ApplyDamage", damage);
		//}
	}
}
