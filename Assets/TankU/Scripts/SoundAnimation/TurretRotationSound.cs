﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretRotationSound : MonoBehaviour {

	public AudioSource emitterAudioSource;

	private void Awake()
	{
	
	}
	// Update is called once per frame
	void Update ()
	{
		if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0)
		 {
			if (!emitterAudioSource.isPlaying)
			{
				emitterAudioSource.Play();
			}
		}
		else
		{
			emitterAudioSource.Pause();
		}		
	}
}
