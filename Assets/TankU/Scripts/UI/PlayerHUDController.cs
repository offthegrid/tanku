﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace TankU
{

	public class PlayerHUDController : Photon.PunBehaviour
	{

		public Text healthValue;
		public Color warningColor;
		public Color okColor;

		public delegate void UpdateHealth(int value);
		public UpdateHealth OnUpdateHealth;

		// Math Info
		public Text timer;
		public Text redScore;
		public Text blueScore;

		public CanvasGroup KillMessagePanel;
		public Text KillMessageEnemyName;

		public float serverStartTime;

		// Use this for initialization
		void Start()
		{
			redScore.text = string.Empty +  "0";
			blueScore.text = string.Empty + "0";

			if (PhotonNetwork.isMasterClient)
			{
				serverStartTime = PhotonNetwork.ServerTimestamp;
				PhotonNetwork.room.SetCustomProperties(new Hashtable() { { GameManager.Instance.ServerStartTimestamp, serverStartTime} });
			}
			else
			{
				serverStartTime = (float) PhotonNetwork.room.CustomProperties[GameManager.Instance.ServerStartTimestamp];
			}

			if(PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.RedTeamScoreKey))
			{
				int redScorePoints = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.RedTeamScoreKey];
				redScore.text = redScorePoints.ToString();
			}

			if (PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.BlueTeamScoreKey))
			{
				int blueScorePoints = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.BlueTeamScoreKey];
				blueScore.text = blueScorePoints.ToString();
			}
		}

		private void OnEnable()
		{
			OnUpdateHealth += UpdateHealthValue;
		}

		private void OnDisable()
		{
			OnUpdateHealth -= UpdateHealthValue;
		}

		// Update is called once per frame
		void Update()
		{

		}

		private void UpdateHealthValue(int value)
		{
			if (value < 30)
			{
				healthValue.color = warningColor;
			}
			else
			{
				healthValue.color = okColor;
			}
			healthValue.text = value.ToString();
		}

		public override void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
		{
			if (propertiesThatChanged.ContainsKey(GameManager.Instance.BlueTeamScoreKey))
			{
				int score = (int)propertiesThatChanged[GameManager.Instance.BlueTeamScoreKey];
				blueScore.text = score.ToString();
			}

			if (propertiesThatChanged.ContainsKey(GameManager.Instance.RedTeamScoreKey))
			{
				int score = (int)propertiesThatChanged[GameManager.Instance.RedTeamScoreKey];
				redScore.text = score.ToString();
			}
		}

		public void PublishYouKilledMessage(string victim)
		{
			Debug.Log("You killed " + victim);
			KillMessagePanel.alpha = 1.0f;
			KillMessageEnemyName.text = victim;
			StartCoroutine("HideAfterTime", KillMessagePanel);
		}

		IEnumerator HideAfterTime(CanvasGroup panelToHide)
		{
			yield return new WaitForSeconds(3.0f);
			panelToHide.alpha = 0.0f;
		}


		//PhotonNetwork.room.SetCustomProperties(new Hashtable() { { PlayerRoomIndexing.RoomPlayerIndexedProp, _indexesLUT} });


	}
}
