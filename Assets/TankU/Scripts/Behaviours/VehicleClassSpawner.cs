﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class VehicleClassSpawner : MonoBehaviour, IVehicleClassProvider
	{
		public GameObject battleTank;

		GameObject IVehicleClassProvider.GetBattleTank()
		{
			return battleTank;
		}
	}
}
