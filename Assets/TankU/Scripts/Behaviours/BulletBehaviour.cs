﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class BulletBehaviour : Photon.MonoBehaviour
	{
		[Tooltip("Only triggers inside this layer will receive damage")]
		public LayerMask tankMask;
		public float lifeTime = 5.0f;
		public float bulletForce = 100.0f;
		public float explosionOffset = 5.0f;
		public float explosionLifeTime = 5.0f;
		[Tooltip("Set to true if it's an artillry bullet")]
		public bool isBallistic = false;
		[Tooltip("Players inside this range will receive damage")]
		public float damageRange = 1.0f;
		[Tooltip("Max damage that the bullet can give if you are in the center of the damageRange")]
		public int fullDamage = 30;
		[Tooltip("As damage is relative to distance to center, this is the minimum rate of damage applyed to players just inside the range")]
		public float minDamageRatio = 0.65f;

		public GameObject explosion;
		private Rigidbody rb;

		public int bulletOwner;

		void Start()
		{
			// If it isn't destroyed by then, destroy the shell after it's lifetime.
			Destroy(gameObject, lifeTime);
			rb = GetComponent<Rigidbody>();

			Vector3 force = transform.forward * bulletForce;
			rb.AddForce(force, ForceMode.VelocityChange);
		}

		private void OnTriggerEnter(Collider other)
		{
			// Deploy visual FX
			Vector3 impact = other.ClosestPoint(transform.position);
			GameObject explosionObject;

			if (isBallistic)
			{
				explosionObject = Instantiate(explosion, impact, Quaternion.Euler(Vector3.up));
			}
			else
			{
				explosionObject = Instantiate(explosion, impact - transform.forward.normalized * explosionOffset, Quaternion.Euler(-transform.forward));
			}

			// if we are master client we establish the damage logic
			if (PhotonNetwork.isMasterClient)
			{
				// Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
				Collider[] colliders = Physics.OverlapSphere(impact, damageRange, tankMask);

				for (int i = 0; i < colliders.Length; i++)
				{
					//if (colliders[i].name != "ReceiveDamage")
					//{
					//	continue;
					//}

					HealthComponent healthComponent = colliders[i].GetComponentInParent<HealthComponent>();

					// If they don't have a healthComponent, go on to the next collider.
					if (!healthComponent)
						continue;

					if (bulletOwner == healthComponent.photonView.owner.ID)
					{
						continue; // don't shoot yourself
					}

					// Calculate the amount of damage the target should take based on it's distance from the shell.
					int damage = CalculateDamage(impact, colliders[i].transform.position);

					healthComponent.photonView.RPC("ReceiveDamage", PhotonTargets.AllViaServer, damage, bulletOwner);
					//PhotonNetwork.SendOutgoingCommands();
				}
			}

			Destroy(explosionObject, explosionLifeTime);
			Destroy(gameObject);
		}

		private int CalculateDamage(Vector3 impact, Vector3 targetPosition)
		{
			// Create a vector from the impact point to the target.
			Vector3 explosionToTarget = targetPosition - impact;

			// Calculate the distance from the shell to the target.
			float explosionDistance = explosionToTarget.magnitude;

			// Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
			float relativeDistance = (damageRange - explosionDistance) / damageRange;

			// Calculate damage as this proportion of the maximum possible damage.
			int damage = Mathf.RoundToInt((minDamageRatio + relativeDistance) * fullDamage); //at least half is guardanteed

			// Make sure that the minimum damage is always 0.
			damage = Mathf.Clamp(damage, 0, fullDamage);

			return damage;
		}
	}

}

