﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimRotateBehaviour : MonoBehaviour
{

	public float speed;
	public Vector3 localAxis;


	// Update is called once per frame
	void Update()
	{
		transform.Rotate(localAxis, speed * Time.deltaTime, Space.Self);
	}
}
