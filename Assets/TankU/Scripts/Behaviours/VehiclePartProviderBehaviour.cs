﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class VehiclePartProviderBehaviour : MonoBehaviour, IVehiclePartProvider
	{
		public Transform vehicleRoot;
		public Transform vehicleChassis;
		public Transform vehicleTurret;
		public Transform vehicleBarrel;
		public Transform vehicleBarrelOut;

		Transform IVehiclePartProvider.getRootTransform()
		{
			return vehicleRoot;
		}

		Transform IVehiclePartProvider.getTurretTransform()
		{
			return vehicleTurret;
		}

		Transform IVehiclePartProvider.getBarrelTransform()
		{
			return vehicleBarrel;
		}

		Transform IVehiclePartProvider.getChassisTransform()
		{
			return vehicleChassis;
		}

		Transform IVehiclePartProvider.getBarrelOutTransform()
		{
			return vehicleBarrelOut;
		}
	}
}
