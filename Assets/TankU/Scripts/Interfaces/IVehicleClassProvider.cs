using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public interface IVehicleClassProvider
	{
		GameObject GetBattleTank();
	}
}
