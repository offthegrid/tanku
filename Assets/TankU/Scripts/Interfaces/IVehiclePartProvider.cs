﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public interface IVehiclePartProvider
	{
		Transform getRootTransform();   // node representing the vehicle position and rotation
		Transform getChassisTransform();// node where the mesh root is located (not the character controller)
		Transform getTurretTransform(); // node representing the turret position and rotation
		Transform getBarrelTransform(); // node representing the barrel position and rotation
		Transform getBarrelOutTransform(); // node where the bullets spawns
	}
}
