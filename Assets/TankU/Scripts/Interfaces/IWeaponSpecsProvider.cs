using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public interface IWeaponSpecsProvider
	{
		float GetRange();
	}
}
