using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public interface ICameraLimitsProvider
	{
		float GetMaxElevation();
		float GetMinElevation();
		float GetMaxRotation();
	}
}
