﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Hashtable = ExitGames.Client.Photon.Hashtable;

namespace TankU
{
	public class GameManager : Photon.PunBehaviour
	{
		static public GameManager Instance; // singleton evilness...but we are prototyping

		public string BlueTeamScoreKey = "BlueTeamScore";
		public string RedTeamScoreKey = "RedTeamScore";
		public string ServerStartTimestamp = "ServerStartTimestamp";

		public GameObject[] tankClassPrefabs;
		public Transform[] respawnRed;
		public Transform[] respawnBlue;	

		public Color redTeamColor;
		public Color blueTeamColor;

		public enum TankClass
		{
			BattleTank = 0,
			EngineerTank,
			ArtilleryTank
		};

		public override void OnLeftRoom()
		{
			SceneManager.LoadScene(0);
		}

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			if (LocalPlayerInstance.LocalPlayer == null) // if it's not null we are reloading the scene and carrying over the Player that is DontDestroyOnLoad
			{
				AssignTeam();
				RespawnPlayer();
			}
		}

		private void UnspawnPlayer()
		{
			PhotonNetwork.Destroy(LocalPlayerInstance.LocalPlayer);
		}

		private void RespawnPlayer()
		{
			PunTeams.Team team = GetTeam();
			Transform respawn = GetRespawnPoint(team);
			TankClass tankClass = GetTankClass();
			LocalPlayerInstance.LocalPlayer = PhotonNetwork.Instantiate(tankClassPrefabs[(int)tankClass].name, respawn.position, respawn.rotation, 0);
		}

		private void AssignTeam()
		{
			int blueTeamCount = 0;
			int redTeamCount = 0;

			if (PunTeams.PlayersPerTeam.ContainsKey(PunTeams.Team.blue))
			{
				blueTeamCount = PunTeams.PlayersPerTeam[PunTeams.Team.blue].Count;
			}
			if (PunTeams.PlayersPerTeam.ContainsKey(PunTeams.Team.red))
			{
				redTeamCount = PunTeams.PlayersPerTeam[PunTeams.Team.red].Count;
			}

			if (blueTeamCount > redTeamCount)
			{
				SetTeam(PunTeams.Team.red);
			}

			else if (redTeamCount > blueTeamCount)
			{
				SetTeam(PunTeams.Team.blue);
			}

			else
			{
				SetTeam((PunTeams.Team)Random.Range(1, 3));
			}
		}

		private void SetTeam(PunTeams.Team team)
		{
			PhotonNetwork.player.SetTeam(team);
		}

		private PunTeams.Team GetTeam()
		{
			return PhotonNetwork.player.GetTeam();
		}

		private TankClass GetTankClass()
		{
			return (TankClass)PlayerPrefs.GetInt("VehicleClassSelected", 0);
		}

		private void SetTankClass(TankClass tankClass)
		{
			PlayerPrefs.SetInt("VehicleClassSelected", (int)tankClass);
		}

		private GameObject GetVehiclePrefab(TankClass tank)
		{
			return tankClassPrefabs[(int)tank];
		}

		private Transform GetRespawnPoint(PunTeams.Team team)
		{
			// TODO: check for spawn being free....
			Transform respawn;
			if (team == PunTeams.Team.red)
			{
				respawn = respawnRed[Random.Range(0, respawnRed.Length)];
			}
			else
			{
				respawn = respawnBlue[Random.Range(0, respawnBlue.Length)];
			}

			return respawn;

		}

		public void UnspawnThenRespawn()
		{
			UnspawnPlayer();
			RespawnPlayer();
		}

		private void Update()
		{
			if (Input.GetKeyUp(KeyCode.Tab))
			{
				LeaveRoom();
			}

			if (Input.GetKeyUp(KeyCode.R))
			{
				UnspawnThenRespawn();
			}

			// Debug pick class
			if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
			{
				SetTankClass(TankClass.BattleTank);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
			{
				SetTankClass(TankClass.EngineerTank);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
			{
				SetTankClass(TankClass.ArtilleryTank);
			}
			//else if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
			//{
			//	SetTankClass(TankClass.RadarTank);
			//}

			// Debug force pick team
			if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8))
			{
				SetTeam(PunTeams.Team.red);
			}
			else if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9))
			{
				SetTeam(PunTeams.Team.blue);
			}
		}

		public int GetTeamScore(PunTeams.Team team)
		{
			int totalScore = 0;

			if (team == PunTeams.Team.red && PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.RedTeamScoreKey))
			{
				totalScore = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.RedTeamScoreKey];
			}

			else if(team == PunTeams.Team.blue && PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.BlueTeamScoreKey))
			{ 
				totalScore = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.BlueTeamScoreKey];
			}

			return totalScore;
		}

		public void IncreaseTeamScore(PunTeams.Team team)
		{
			int currentScore = 0;

			// Read current
			if (team == PunTeams.Team.red && PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.RedTeamScoreKey))
			{
				currentScore = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.RedTeamScoreKey];
			}

			else if (team == PunTeams.Team.blue && PhotonNetwork.room.CustomProperties.ContainsKey(GameManager.Instance.BlueTeamScoreKey))
			{
				currentScore = (int)PhotonNetwork.room.CustomProperties[GameManager.Instance.BlueTeamScoreKey];
			}

			// Update score
			if (team == PunTeams.Team.red)
			{
				PhotonNetwork.room.SetCustomProperties(new Hashtable() { { GameManager.Instance.RedTeamScoreKey, currentScore + 1 } }); //, new Hashtable() { { GameManager.Instance.RedTeamScoreKey, currentScore } } );
			}
			else if (team == PunTeams.Team.blue)
			{
				PhotonNetwork.room.SetCustomProperties(new Hashtable() { { GameManager.Instance.BlueTeamScoreKey, currentScore + 1 } }); //, new Hashtable() { { GameManager.Instance.RedTeamScoreKey, currentScore } } );
			}
		}

		public void LeaveRoom()
		{
			PhotonNetwork.LeaveRoom();
		}

		void LoadMap()
		{
			if (!PhotonNetwork.isMasterClient)
			{
				Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
			}
			Debug.Log("PhotonNetwork : Loading Level : " + PhotonNetwork.room.PlayerCount);
			PhotonNetwork.LoadLevel("ShootingRange");
		}

		public override void OnPhotonPlayerConnected(PhotonPlayer other)
		{
			Debug.Log("OnPhotonPlayerConnected() " + other.NickName); // not seen if you're the player connecting

			if (PhotonNetwork.isMasterClient)
			{
				Debug.Log("OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected

				//LoadMap();
			}
		}

		public override void OnPhotonPlayerDisconnected(PhotonPlayer other)
		{
			Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName); // seen when other disconnects

			if (PhotonNetwork.isMasterClient)
			{
				Debug.Log("OnPhotonPlayerDisonnected isMasterClient " + PhotonNetwork.isMasterClient); // called before OnPhotonPlayerDisconnected
																									   //LoadMap();
			}

		}
	}
}
