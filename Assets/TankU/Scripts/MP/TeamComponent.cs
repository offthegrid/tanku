﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class TeamComponent : Photon.PunBehaviour
	{
		public Material redChassisMaterial;
		public Material blueChassisMaterial;
		public Material redTurretMaterial;
		public Material blueTurretMaterial;
		public Material redBarrelMaterial;
		public Material blueBarrelMaterial;

		void Awake()
		{

		}

		// Use this for initialization
		void Start()
		{
			IVehiclePartProvider partProvider = (IVehiclePartProvider)GetComponent(typeof(IVehiclePartProvider));

			if (photonView.owner.GetTeam() == PunTeams.Team.red)
			{
				SetTeamColorToRenderer(partProvider.getChassisTransform().GetComponent<Renderer>(), redChassisMaterial);
				SetTeamColorToRenderer(partProvider.getTurretTransform().GetComponent<Renderer>(), redTurretMaterial);
				SetTeamColorToRenderer(partProvider.getBarrelTransform().GetComponent<Renderer>(), redBarrelMaterial);
			}

			else if (photonView.owner.GetTeam() == PunTeams.Team.blue)
			{
				SetTeamColorToRenderer(partProvider.getChassisTransform().GetComponent<Renderer>(), blueChassisMaterial);
				SetTeamColorToRenderer(partProvider.getTurretTransform().GetComponent<Renderer>(), blueTurretMaterial);
				SetTeamColorToRenderer(partProvider.getBarrelTransform().GetComponent<Renderer>(), blueBarrelMaterial);
			}

		}

		private void SetTeamColorToRenderer(Renderer renderer, Material material)
		{
			if (renderer == null)
			{
				return;
			}

			var mats = renderer.materials;
			mats[0] = material;
			renderer.materials = mats;
		}

		// Update is called once per frame
		void Update()
		{

		}
	}
}
