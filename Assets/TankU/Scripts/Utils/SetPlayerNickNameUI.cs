﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{


	public class SetPlayerNickNameUI : Photon.MonoBehaviour
	{

		public TextMesh playerName;

		// Use this for initialization
		void Start()
		{
			if (photonView.isMine)
			{
				playerName.gameObject.SetActive(false);
				//playerName.text = photonView.owner.NickName;
			}
			else
			{
				if (photonView.owner.GetTeam() == PunTeams.Team.blue)
				{
					playerName.color = GameManager.Instance.blueTeamColor;
				}
				else if (photonView.owner.GetTeam() == PunTeams.Team.red)
				{
					playerName.color = GameManager.Instance.redTeamColor;
				}

				playerName.text = photonView.owner.NickName;
			}
		}
	}
}

