﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TankU
{
	public class LocalPlayerInstance : Photon.MonoBehaviour
	{
		public static GameObject LocalPlayer;

		void Awake()
		{
			if (photonView.isMine)
			{
				LocalPlayerInstance.LocalPlayer = this.gameObject;
			}

			DontDestroyOnLoad(this.gameObject);
		}
	}
}