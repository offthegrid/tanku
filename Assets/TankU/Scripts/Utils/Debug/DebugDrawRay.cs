﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugDrawRay : MonoBehaviour
{
	public Transform drawInfo;
	public float rayExtension = 10.0f;
	public Color rayColor = Color.red;

	void Update()
	{
		Vector3 forward = drawInfo.TransformDirection(Vector3.forward) * rayExtension;
		Debug.DrawRay(drawInfo.position, forward, rayColor);
	}
}
