﻿using UnityEngine;
using System.Collections;

namespace TankU
{
	[CreateAssetMenu(fileName = "WeaponData", menuName = "TankU/Data/Weapon")]
	public class WeaponDataClass : ScriptableObject
	{
		[Tooltip("Weapon range")]
		public float range;
		[Tooltip("[Seconds] The time the weapon cannot shoot after firing (the inverse of the fire rate)")]
		public float coolDownTime;
		[Tooltip("Collection of audio clips that will be played randomly for each shoot")]
		public AudioClip[] fireClips;
		[Tooltip("GameObject holding the fire effect that will be instantiated (only visual)")]
		public GameObject fireEffect;
		[Tooltip("Max lifetime for the effect before being destroyed")]
		public float fireEffectLifeTime;

	}
}
