using UnityEngine;
using System.Collections;

namespace TankU
{
	[CreateAssetMenu(fileName = "BulletData", menuName = "TankU/Data/Bullet")]
	public class BulletDataClass : ScriptableObject
	{
		public GameObject bullet;
	}
}
