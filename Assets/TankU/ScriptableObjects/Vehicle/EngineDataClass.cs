﻿using UnityEngine;
using System.Collections;

namespace TankU
{
    [CreateAssetMenu(fileName = "EngineData", menuName = "TankU/Data/Engine")]
    public class EngineDataClass : ScriptableObject
    {
        [Tooltip("Speed when moving forward")]
        public float speed;
        [Tooltip("Speed when moving backwards (reverse gear)")]
        public float reversingSpeed;
        [Tooltip("Speed when curving")]
        public float turningSpeed;
        [Tooltip("Speed when rotating in place (only tracked vehicles)")]
        public float rotatingSpeed;
    }
}
