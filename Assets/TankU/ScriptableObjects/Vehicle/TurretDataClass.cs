using UnityEngine;
using System.Collections;

namespace TankU
{
    [CreateAssetMenu(fileName = "TurretData", menuName = "TankU/Data/Turret")]
    public class TurretDataClass : ScriptableObject
    {
        [Tooltip("[Degrees] Maximum elevation the barrel reaches when looks up (rotation along its local X)")]
        public float maxElevation;
        [Tooltip("[Degrees] Minimum elevation the barrel reaches when looks down (rotation along its local X)")]
        public float minElevation;
        [Tooltip("[Degrees] Maximum horizontal rotation the turret can do. It will be maxRotaion/2 to the left and maxRotation/2 to the right. 360=infinite rotation")]
        public float maxRotation;

    }
}
