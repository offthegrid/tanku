﻿using UnityEngine;
using System.Collections;

namespace TankU
{
	[CreateAssetMenu(fileName = "CameraOrbitData", menuName = "TankU/Data/CameraOrbit")]
	public class CameraOrbitDataClass : ScriptableObject
	{
		[Tooltip("Orbit distance from the pivot")]
		public float distance;
		[Tooltip("Orbit height from the equator")]
		public float height;
		[Tooltip("Orbit horizontal speed (along a parallel)")]
		public float horizontalSpeed;
		[Tooltip("Orbit horizontal speed (along a meridian)")]
		public float verticalSpeed;
	}
}
